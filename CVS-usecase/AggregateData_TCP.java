// ------------------------------------------------------------------------
// Author: Salman Taherizadeh
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1]: IP address where the data processing part is running
// args[2...n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
import org.json.JSONObject;
import java.util.Scanner;
// ------------------------------------------------------------------------
class AggregateData_TCP {
	public static void main(String args[]) throws Exception {
		try{
			// ------------------------------------------------------------------------
			Socket clientSocket = new Socket(args[1], Integer.parseInt(args[0])); // such as "127.0.0.1" - 6789
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			// ------------------------------------------------------------------------
			Scanner scanner = new Scanner(new File(args[2])); // "RAW_ACCELEROMETERS.csv"
			scanner.useDelimiter(",");
			///////////////
			int Y_column = Integer.parseInt(args[3]); // 6
			int Z_column = Integer.parseInt(args[4]); // 7
			int Interval = Integer.parseInt(args[5]); //Interval: 100 ms
			///////////////
			while(scanner.hasNext()){
				long lasttime = System.currentTimeMillis();
				String line = scanner.nextLine();
				String str_current_line [] = line.split(",");
				int Time_in_second = (int) (Double.parseDouble(str_current_line[0]));
				double Y_acceleration = Double.parseDouble(str_current_line[Y_column]);
				double Z_acceleration = Double.parseDouble(str_current_line[Z_column]);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("Time_in_second", Time_in_second);
				jsonObject.put("Y_acceleration", Y_acceleration);
				jsonObject.put("Z_acceleration", Z_acceleration);
				outToServer.writeBytes(jsonObject.toString()+ '\n');
				long curtime = System.currentTimeMillis(); 
				Thread.sleep(Math.abs(Interval - (curtime-lasttime)));
			}
			// ------------------------------------------------------------------------
			clientSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}