// ------------------------------------------------------------------------
// Author: Salman Taherizadeh
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1...n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
import org.json.JSONObject;
import java.nio.charset.StandardCharsets;
// ------------------------------------------------------------------------
public class ProcessData_TCP {

	public static void main(String[] args) throws Exception {
		try {
			// ------------------------------------------------------------------------
			ServerSocket welcomeSocket = new ServerSocket(Integer.parseInt(args[0])); // such as 6789
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			// ------------------------------------------------------------------------
			String DB_IP = args[2];
			String Driver_ID = args[3];
			///////////////
			double Threshold = Double.parseDouble(args[1]); // 0.3 -> To recognise if a sample would be an occurance. A sample is an occurance, if its value is beyong the threshold.
			int MIN = 3; // Minimum number of consecutive occurance to recognise an event: MIN
			int MAX = MIN * 6; // Maximum number of consecutive occurance to recognise an event: MAX
			int NoIgSamp = MIN * 2; // Number of samples (which are below the threshold and occur after a recognised event) that can be ignored: NoIgSamp
			int delay = 3; // To be set to zero
			///////////////
			double local_Y_max = 1000;
			int Y_cont_occurrence = 0;
			int Y_cont_ignored = 0;
			double previous_Y_acceleration = 0.0;
			///////////////
			double local_Z_max = 1000;
			int Z_cont_occurrence = 0;
			int Z_cont_ignored = 0;
			double previous_Z_acceleration = 0.0;
			///////////////
			while (true){
				String clientSentence = inFromClient.readLine();
				JSONObject jsonObject = new JSONObject(clientSentence);
				int Time_in_second = (int) jsonObject.get("Time_in_second");
				//////////////////////////////////////////Start of Y acceleration process
				double current_Y_acceleration = (double) jsonObject.getDouble("Y_acceleration");
				if (local_Y_max == 1000) local_Y_max = current_Y_acceleration;
				if ((previous_Y_acceleration>0)&&(current_Y_acceleration>0)){
					if ((Y_cont_occurrence<MIN) && (current_Y_acceleration>=Threshold)){
						Y_cont_occurrence++;
						local_Y_max=(current_Y_acceleration>local_Y_max)?current_Y_acceleration:local_Y_max;
						if ((Y_cont_occurrence==MIN)&&(Y_cont_ignored==0)){
							Notification_DB(1, Time_in_second, delay, DB_IP, Driver_ID); //Notification(1, Time_in_second, delay);
						}
					}
					if ((Y_cont_occurrence<MIN) && (current_Y_acceleration<Threshold)){
						Y_cont_occurrence=0;
						Y_cont_ignored=0;
					}
					if ((Y_cont_ignored<NoIgSamp) && (Y_cont_occurrence>=MIN) && (current_Y_acceleration<Threshold)){
						Y_cont_ignored++;
					}
					if ((Y_cont_ignored>=NoIgSamp) && (Y_cont_occurrence>=MIN) && (current_Y_acceleration<Threshold)){
						Y_cont_occurrence=0;
						Y_cont_ignored=0;
						local_Y_max=1000;
					}
					if ((Y_cont_occurrence>=MIN) && (current_Y_acceleration>=Threshold)){
						Y_cont_occurrence++;
						if (Y_cont_occurrence==MAX){
							Y_cont_occurrence=0;
							Y_cont_ignored=0;
							local_Y_max=1000;
						}
					}
				}
				else if((previous_Y_acceleration<0)&&(current_Y_acceleration<0)){
					if ((Y_cont_occurrence<MIN) && (current_Y_acceleration<=-Threshold)){
						Y_cont_occurrence++;
						local_Y_max=(current_Y_acceleration<local_Y_max)?current_Y_acceleration:local_Y_max;
						if ((Y_cont_occurrence==MIN)&&(Y_cont_ignored==0)){
							Notification_DB(2, Time_in_second, delay, DB_IP, Driver_ID); //Notification(2, Time_in_second, delay);
						}
					}
					if ((Y_cont_occurrence<MIN) && (current_Y_acceleration>Threshold)){
						Y_cont_occurrence=0;
						Y_cont_ignored=0;
					}
					if ((Y_cont_ignored<NoIgSamp) && (Y_cont_occurrence>=MIN) && (current_Y_acceleration>Threshold)){
						Y_cont_ignored++;
					}
					if ((Y_cont_ignored>=NoIgSamp) && (Y_cont_occurrence>=MIN) && (current_Y_acceleration>Threshold)){
						Y_cont_occurrence=0;
						Y_cont_ignored=0;
						local_Y_max=1000;
					}
					if ((Y_cont_occurrence>=MIN) && (current_Y_acceleration<=-Threshold)){
						Y_cont_occurrence++;
						if (Y_cont_occurrence==MAX){
							Y_cont_occurrence=0;
							Y_cont_ignored=0;
							local_Y_max=1000;
						}
					}
				}
				else if((previous_Y_acceleration>0)&&(current_Y_acceleration<0)){
					// Nothing
				}
				else if((previous_Y_acceleration<0)&&(current_Y_acceleration>0)){
					// Nothing
				}
				previous_Y_acceleration = current_Y_acceleration;
				//////////////////////////////////////////Start of Z acceleration process
				double current_Z_acceleration = (double) jsonObject.getDouble("Z_acceleration");
				if (local_Z_max == 1000) local_Z_max = current_Z_acceleration;
				if ((previous_Z_acceleration>0)&&(current_Z_acceleration>0)){
					if ((Z_cont_occurrence<MIN) && (current_Z_acceleration>=Threshold)){
						Z_cont_occurrence++;
						local_Z_max=(current_Z_acceleration>local_Z_max)?current_Z_acceleration:local_Z_max;
						if ((Z_cont_occurrence==MIN)&&(Z_cont_ignored==0)){
							Notification_DB(3, Time_in_second, delay, DB_IP, Driver_ID); //Notification(3, Time_in_second, delay);
						}
					}
					if ((Z_cont_occurrence<MIN) && (current_Z_acceleration<Threshold)){
						Z_cont_occurrence=0;
						Z_cont_ignored=0;
					}
					if ((Z_cont_ignored<NoIgSamp) && (Z_cont_occurrence>=MIN) && (current_Z_acceleration<Threshold)){
						Z_cont_ignored++;
					}
					if ((Z_cont_ignored>=NoIgSamp) && (Z_cont_occurrence>=MIN) && (current_Z_acceleration<Threshold)){
						Z_cont_occurrence=0;
						Z_cont_ignored=0;
						local_Z_max=1000;
					}
					if ((Z_cont_occurrence>=MIN) && (current_Z_acceleration>=Threshold)){
						Z_cont_occurrence++;
						if (Z_cont_occurrence==MAX){
							Z_cont_occurrence=0;
							Z_cont_ignored=0;
							local_Z_max=1000;
						}
					}
				}
				else if((previous_Z_acceleration<0)&&(current_Z_acceleration<0)){
					if ((Z_cont_occurrence<MIN) && (current_Z_acceleration<=-Threshold)){
						Z_cont_occurrence++;
						local_Z_max=(current_Z_acceleration<local_Z_max)?current_Z_acceleration:local_Z_max;
						if ((Z_cont_occurrence==MIN)&&(Z_cont_ignored==0)){
							Notification_DB(4, Time_in_second, delay, DB_IP, Driver_ID); //Notification(4, Time_in_second, delay);
						}
					}
					if ((Z_cont_occurrence<MIN) && (current_Z_acceleration>Threshold)){
						Z_cont_occurrence=0;
						Z_cont_ignored=0;
					}
					if ((Z_cont_ignored<NoIgSamp) && (Z_cont_occurrence>=MIN) && (current_Z_acceleration>Threshold)){
						Z_cont_ignored++;
					}
					if ((Z_cont_ignored>=NoIgSamp) && (Z_cont_occurrence>=MIN) && (current_Z_acceleration>Threshold)){
						Z_cont_occurrence=0;
						Z_cont_ignored=0;
						local_Z_max=1000;
					}
					if ((Z_cont_occurrence>=MIN) && (current_Z_acceleration<=-Threshold)){
						Z_cont_occurrence++;
						if (Z_cont_occurrence==MAX){
							Z_cont_occurrence=0;
							Z_cont_ignored=0;
							local_Z_max=1000;
						}
					}
				}
				else if((previous_Z_acceleration>0)&&(current_Z_acceleration<0)){
					// Nothing
				}
				else if((previous_Z_acceleration<0)&&(current_Z_acceleration>0)){
					// Nothing
				}
				previous_Z_acceleration = current_Z_acceleration;
				//////////////////////////////////////////
			}
		// ------------------------------------------------------------------------
		} catch (Exception e) {
			//e.printStackTrace();
		}
    }
	//////////////////////////////
	public static void Notification(int type, int Time_in_second, int delay) throws Exception {
		try {
			System.out.println("------------------");
			if (type==1) System.out.println("Event: " + "Aggressive Right");
			else if (type==2) System.out.println("Event: " + "Aggressive Left");
			else if (type==3) System.out.println("Event: " + "Sudden Acceleration");
			else if (type==4) System.out.println("Event: " + "Sudden Braking");
			int temp = Time_in_second + delay;
			int min = (int) (temp/60); //m
			int sec = temp - min *60;
			System.out.println("Time: " + min+ ":" + sec);
			System.out.println("------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//////////////////////////////
	public static void Notification_DB(int type, int Time_in_second, int delay, String DB_IP, String Driver_ID) throws MalformedURLException, ProtocolException, IOException {
		String type_str = "";
		if (type==1) type_str = "Aggressive Right";
		else if (type==2) type_str = "Aggressive Left";
		else if (type==3) type_str = "Sudden Acceleration";
		else if (type==4) type_str = "Sudden Braking";
		int temp = Time_in_second + delay;
		int min = (int) (temp/60); //m
		int sec = temp - min *60;
		HttpURLConnection con;
		String url = "http://" + DB_IP + ":8080/inserting.php";
        String urlParameters = "driverid=" + Driver_ID + "&event=" + type_str + "&min_event=" + String.valueOf(min) + "&sec_event=" + String.valueOf(sec);
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        
		URL myurl = new URL(url);
		con = (HttpURLConnection) myurl.openConnection();
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(postData);
		}
		StringBuilder content;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
			String line;
			content = new StringBuilder();
			while ((line = in.readLine()) != null) {
				content.append(line);
				content.append(System.lineSeparator());
			}
		}
		//System.out.println(content.toString());
		con.disconnect();
	}
	//////////////////////////////
}