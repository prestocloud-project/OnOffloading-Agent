#!/bin/bash

if [ $Process == "yes" ]&&[ $Aggregate == "yes" ]; then
	java ProcessData_TCP $ServicePort $Threshold $DB_IP $Driver_ID &
	exec java AggregateData_TCP $ServicePort $ProcessIP $FileName $Y_column $Z_column $Interval
fi

if [ $Process == "no" ]&&[ $Aggregate == "yes" ]; then
	exec java AggregateData_TCP $ServicePort $ProcessIP $FileName $Y_column $Z_column $Interval
fi

if [ $Process == "yes" ]&&[ $Aggregate == "no" ]; then
	exec java ProcessData_TCP $ServicePort $Threshold $DB_IP $Driver_ID
fi


