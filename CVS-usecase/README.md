# CVS logistic use case
<p align="justify">
The CVS logistic use case presents a transport logistic system with advanced real-time data-intensive analytics on telematics data streams (e.g. acceleration, breaking speed limit, concentration, manoeuvring, etc.) at the edge of the network. This system extracts important information through real-time analytics which will allow better manual, semi-manual or even automatic decision making. The CVS logistic system is aimed at addressing functional needs of stakeholders (e.g. driver, logistic centre, vehicle owner, and so on).

http://www.cvs-mobile.com/

</p>
