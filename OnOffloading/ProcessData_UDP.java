// ------------------------------------------------------------------------
// Author: Salman Taherizadeh - Jozef Stefan Institute (JSI)
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1],...,args[n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
....... application-specific libraries .......
// ------------------------------------------------------------------------
public class ProcessData_UDP {
	public static void main(String[] args) throws Exception {
		try {
			// ------------------------------------------------------------------------
			DatagramSocket  welcomeSocket = new DatagramSocket(Integer.parseInt(args[0])); // such as 6789
			byte[] receiveData = new byte[1024];
			byte[] sendData = new byte[1024];
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			// ------------------------------------------------------------------------
      ....... application-specific code .......
		// ------------------------------------------------------------------------
		} catch (Exception e) {
			//e.printStackTrace();
		}
    }
}
