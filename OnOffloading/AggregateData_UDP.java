// ------------------------------------------------------------------------
// Author: Salman Taherizadeh - Jozef Stefan Institute (JSI)
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1]: IP address where the data processing part is running
// args[2],...,args[n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
....... application-specific libraries .......
// ------------------------------------------------------------------------
class AggregateData_UDP {
	public static void main(String args[]) throws Exception {
		try{
			// ------------------------------------------------------------------------
			InetAddress IPAddress = InetAddress.getByName(args[1]); // such as "127.0.0.1"
			DatagramSocket clientSocket = new DatagramSocket();
			byte[] sendData = new byte[1024];
			byte[] receiveData = new byte[1024];
			// ------------------------------------------------------------------------
			....... application-specific code .......
		// ------------------------------------------------------------------------
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
