#!/bin/bash

if [ $Process == "yes" ]&&[ $Aggregate == "yes" ]; then
	java ProcessData_UDP $ServicePort $A $B ... $Z &
	exec java AggregateData_UDP $ServicePort $ProcessIP $a $b ... $z
fi

if [ $Process == "no" ]&&[ $Aggregate == "yes" ]; then
	exec java AggregateData_UDP $ServicePort $ProcessIP $a $b ... $z
fi

if [ $Process == "yes" ]&&[ $Aggregate == "no" ]; then
	exec java ProcessData_UDP $ServicePort $A $B ... $Z
fi


