// ------------------------------------------------------------------------
// Author: Salman Taherizadeh - Jozef Stefan Institute (JSI)
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1],...,args[n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
....... application-specific libraries .......
// ------------------------------------------------------------------------
public class ProcessData_TCP {
	public static void main(String[] args) throws Exception {
		try {
			// ------------------------------------------------------------------------
			ServerSocket welcomeSocket = new ServerSocket(Integer.parseInt(args[0])); // such as 6789
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			// ------------------------------------------------------------------------
			....... application-specific code .......
		// ------------------------------------------------------------------------
		} catch (Exception e) {
			//e.printStackTrace();
		}
    }
}
