# On/Offloading Agent
<p align="justify">
An On/Offloading Agent is installed on every edge resource. On/Offloading Agent is responsible for registering the edge node in the Cloud and Edge Resources Repository through On/Offload Processing component. Moreover, it responds to the Mobile On/Offload Processing component’s requests for the on/offloading tasks which can be start or stop of application between edge nodes and cloud-based datacenters. Such communication between the Mobile On/Offload Processing component and On/Offloading Agent is made via the Message Broker.
</p>
######################################<br/>
Instructions for use of On/Offloading Agent<br/>

## Step 1- Typical scenario

https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/Typical-Scenario.md

## Step 2- Input of On/Offloading Agent

https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/Input.md

## Step 3- Aggregating data part
<p align="justify">
The communication protocol used to transfer the data between the aggregating data part and the processing data part can be either TCP or UDP, depending on the application type. This communication can be implemented via different mechanisms such as socket programming. In order to develop the aggregating data part, a Java file needs to be coded.
</p>
If the application needs TCP protocol, the following Java file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/AggregateData_TCP.java

<br/><br/>
If the application needs UDP protocol, the following Java file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/AggregateData_UDP.java

<br/><br/>
Note 1: In the Java code offered above, `args[2]`,...,`args[n]` are application-specific inputs, if any. It should be noted that these inputs are determined at the end of the request which is sent to the On/Offloading Agent, explained before in Step 2. The application developer should define which inputs need to be determined for the application.

Note 2: In the Java code offered above, there is a part which is highlighted as `....... application-specific libraries .......`. In this part, the application developer should import all libraries which are application-specific and required for the application.


Note 3: In the Java code offered above, there is a part which is highlighted as `....... application-specific code .......`. This part is application-specific and it should be provided by the application developer. For instance, Java code of the CVS logistic application can be seen to get more familiar with the application-specific part.

## Step 4- Processing data part
In order to develop the processing data part, a Java file needs to be coded.

</br>
If the application needs TCP protocol, the following Java file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/ProcessData_TCP.java

<br/><br/>
If the application needs UDP protocol, the following Java file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/ProcessData_UDP.java

<br/>

## Step 5- Shell file
When a container is going to be instantiated, a shell (.sh) file is executed to define which one of aggregating data part or processing data part, or even both should be performed in the container.

</br>
If the application needs TCP protocol, the following shell file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/start_TCP.sh

<br/><br/>
If the application needs UDP protocol, the following shell file can be considered:
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/start_UDP.sh

<br/><br/>
Note 1: Some application-specific inputs which are determined at the end of the request sent to the On/Offloading Agent are used in the aggregating data part, and some of them are used in the processing data part. In the shell code offered above, `$A` `$B` ... `$Z` are the name of application-specific inputs (if any) which are used in the processing data part. In the shell code offered above, `$a` `$b` ... `$z` are the name of application-specific inputs (if any) which are used in the aggregating data part. It should be noted that these names should be exactly the names which are determined at the end of the request (the general string explained in Step 2) sent to the On/Offloading Agent, in terms of uppercase or lowercase. If the application does not have any input, `$A` `$B` ... `$Z` and `$a` `$b` ... `$z` should be eliminated.

Note 2: In the shell code offered above, there is a character `&` which should not be deleted. This character at the end of Java call means that the process will be started in background.

Note 3: For instance, shell code of the CVS logistic application can be seen to get more familiar with application-specific inputs in this part.

## Step 6- Dockerfile
A Dockerfile consists of consecutive steps on how to create a specific Docker image. In other words, A Dockerfile says what base image should be used, which dependencies need to be downloaded, what commands have to run to execute the containerized application, and so on. In order to make the container image which includes both aggregating data part and processing data part, follow these instructions respectively: 

* Make a new folder via `mkdir`, and go inside via `cd`.
* Compile both AggregateData_TCP.java (or AggregateData_UDP.java) and ProcessData_TCP.java (or ProcessData_UDP.java) via `javac` to have AggregateData_TCP.class (or AggregateData_UDP.class) and ProcessData_TCP.class (or ProcessData_UDP.class).
* Make a Dockerfile file without extension:
</br><br/>
If the application needs TCP protocol, the following Dockerfile file can be considered:<br/>
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/Dockerfile_TCP
<br/><br/>
If the application needs UDP protocol, the following Dockerfile file can be considered:<br/>
https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/Dockerfile_UDP
<br/><br/>Note: In the Dockerfile offered above, there is a part which is highlighted as `....... application-specific commands .......`. In this part, the application developer should write all commands which are application-specific and required for the application. For instance, Dockerfile of the CVS logistic application can be seen to get more familiar with the application-specific part.

* Build the container image:
</br><br/>
If the application needs TCP protocol, the following command can be considered:<br/>
`docker build -t salmant/cvs_container_image_tcp -f Dockerfile_TCP .`
<br/><br/>
If the application needs UDP protocol, the following command can be considered:<br/>
`docker build -t salmant/cvs_container_image_udp -f Dockerfile_UDP .`
<br/><br/>Note: Put your repository name instead of `salmant`, and put your container image name instead of `cvs_container_image_udp`.

* Push the built container image on your repository:
</br><br/>
If the application needs TCP protocol, the following command can be considered:<br/>
`docker push salmant/cvs_container_image_tcp`
<br/><br/>
If the application needs UDP protocol, the following command can be considered:<br/>
`docker push salmant/cvs_container_image_udp`

## Step 7- Docker and Docker's Remote API
On every edge node or cloud-based host, Docker engine should be installed, and Docker's Remote API should be enabled. To this end, follow these instructions respectively: 

In order to install Docker and enable Docker's Remote API on a Raspberry Pi 3:
* Execute `sudo apt-get install -y apt-transport-https`.
* Execute `wget -q https://packagecloud.io/gpg.key -O - | sudo apt-key add -`.
* Execute `echo 'deb https://packagecloud.io/Hypriot/Schatzkiste/debian/ wheezy main' | sudo tee /etc/apt/sources.list.d/hypriot.list`.
* Execute `sudo apt-get update`.
* Execute `sudo apt-get install -y docker-hypriot`.
* Execute `sudo systemctl enable docker`.
* Execute `sudo docker version` to confirm that Docker works well.
* Execute `sudo service docker stop`.
* Execute `cd /etc/systemd/system/`.
* Execute `sudo mkdir docker.service.d`.
* Execute `cd docker.service.d`.
* Execute `sudo nano remote-api.conf`.
* Add these three lines to the file:<br/>
`[Service]`<br/>
`ExecStart=`<br/>
`ExecStart=/usr/bin/docker daemon -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock`<br/>
* Execute `sudo systemctl daemon-reload`.
* Execute `sudo service docker restart`.
* Execute `sudo curl -X GET http://127.0.0.1:4243/images/json` to make sure that Docker Remote API works well.

In order to install Docker and enable Docker's Remote API on a cloud-based host:
* Execute `apt-get update`. This command downloads the package lists from the repositories and updates them to get information on the newest versions of packages and their dependencies.
* Execute `apt-get -y install docker.io`. This command installs Docker.
* Execute `ln -sf /usr/bin/docker.io /usr/local/bin/docker`. This command links Docker path.
* Execute `service docker stop`. This command stops Docker engine.
* Add this line `DOCKER_OPTS='-H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock'` to this file `/etc/default/docker`.
* Execute `service docker start`. This command starts Docker engine again.

## Step 8- Running the On/Offloading Agent
Regardless of what communication protocol is used to transfer the data between the aggregating data part and the processing data part (either TCP or UDP), the following code should be compiled to run the On/Offloading Agent deployed on every edge node:

https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/On_Offloading_Client.java

<br/><br/>
Note: In the Java code offered above, the On/Offloading Agent is listening to the port with the number of `10001` to reaceive requests (start or stop requests) sent from the Mobile On/Offload Processing component. Such requests are string sent to the On/Offloading Agent that should be based on what is explained in Step 2.


