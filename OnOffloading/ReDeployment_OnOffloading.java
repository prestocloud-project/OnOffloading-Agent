// ------------------------------------------------------------------------
// Author: Salman Taherizadeh - Jozef Stefan Institute (JSI)
// ------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.net.*;

public class ReDeployment_OnOffloading extends Thread implements Runnable {
	
	public static void main(String arg[]) throws Exception {
		ServerSocket welcomeSocket = new ServerSocket(10001);
		while (true){
			String ServicePort[];
			String HostPort[];
			String tcp_or_udp[];
			String input_name[];
			String input_value[];
			int inputs_count = 0;
			int ports_count = 0;
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient =
			new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			String clientSentence = inFromClient.readLine();
			String[] argv = clientSentence.split(",");
			String IP = argv[1];
			String ContainerImageName = argv[2];
			ports_count= Integer.parseInt(argv[3]);
			ServicePort = new String[ports_count];
			HostPort =  new String[ports_count];
			tcp_or_udp =  new String[ports_count];
			for (int i=0; i < ports_count; i++) {
					ServicePort[i] = argv[4+(i*3)];
					HostPort[i] = argv[5+(i*3)];
					tcp_or_udp[i] = argv[6+(i*3)];
			}
			if (argv[0].equals("start")){
				input_name = new String[argv.length - 4 - (ports_count * 3) + 1 ]; // plus one because of ServicePort
				input_value = new String[argv.length - 4 - (ports_count * 3) + 1 ]; // plus one because of ServicePort
				
				for (int i=(4 + (ports_count * 3)); i < argv.length; i=i+2) if (argv[i]!=null){
					input_name[inputs_count] = argv[i];
					input_value[inputs_count] = argv[i+1];
					inputs_count++;
				}
				input_name[inputs_count] = "ServicePort";
				input_value[inputs_count] = argv[4];
				inputs_count++;
				String Id = create_a_container(IP, ContainerImageName, ServicePort, HostPort, tcp_or_udp, input_name, input_value, inputs_count, ports_count);
				if (Id==null || Id.trim().equals("")) System.out.println("201"); // Code of error in start a container: error in create_a_container()
				else {
					String Result = start_a_container(Id, IP);
					if (!(Result==null || Result.trim().equals(""))) System.out.println("202"); // Code of error in start a container: error in start_a_container()
					else System.out.println("200"); // Code of success in start a container
				}
			} else
			if (argv[0].equals("stop")){
				String Id = get_container_id(ServicePort[0], HostPort[0], tcp_or_udp[0], IP);
				if (Id.equals("")) System.out.println("301"); // Code of error in stop a container
				else {
					stop_a_container(Id, IP);
					System.out.println("300"); // Code of success in stop a container
				}
			}
		}
	}
	
	/////////////////////////////////////////////////////////////
	
	public static String create_a_container(String IP, String ContainerImageName, String ServicePort[], String HostPort[], String tcp_or_udp[], String input_name[], String input_value[], int inputs_count, int ports_count){
        try{
			String mainCommand =
			"	curl -X POST -H \'Content-Type: application/json\' -d \'{	" +
			"	\"Image\":\"" + ContainerImageName + "\",	" ;
			if (inputs_count>0){
				mainCommand += "	\"Env\": [	" ;
				for (int i=0; i < inputs_count; i++) 
					if (i != inputs_count-1) mainCommand +="\"" + input_name[i] + "=" + input_value[i] + "\"," ;
					else mainCommand +="\"" + input_name[i] + "=" + input_value[i] + "\"" ;
				mainCommand += "		   ],	" ;
			}
			mainCommand +=
			"	\"Tty\": true,	" +
			"	\"ExposedPorts\": { " ;
			for (int i=0; i < ports_count; i++)
				if (i != ports_count-1) mainCommand += "\"" + ServicePort[i] + "/" + tcp_or_udp[i] + "\": {},";
				else mainCommand += "\"" + ServicePort[i] + "/" + tcp_or_udp[i] + "\": {}";
			mainCommand += "},	";
			mainCommand +=
			"	\"PortBindings\": { " ;
			for (int i=0; i < ports_count; i++)
				if (i != ports_count-1) mainCommand += "\"" + ServicePort[i] + "/" + tcp_or_udp[i] + "\": [{ \"HostPort\": \"" + HostPort[i] + "\" }],";
				else mainCommand += "\"" + ServicePort[i] + "/" + tcp_or_udp[i] + "\": [{ \"HostIp\": \"\",\"HostPort\": \"" + HostPort[i] + "\" }]";
			mainCommand += "}	" +
			"	}\' http://" + IP + ":4243/containers/create	"
			;
			//System.out.println(mainCommand);
			String[] command={"/bin/bash", "-c", mainCommand};
			Process P = Runtime.getRuntime().exec(command);
            P.waitFor();
            BufferedReader StdInput = new BufferedReader(new InputStreamReader(P.getInputStream()));
            String TopS ="";
			TopS= StdInput.readLine();
			TopS = TopS.substring(6);
			TopS = TopS.substring(TopS.indexOf("\"") + 1);
			TopS = TopS.substring(0, TopS.indexOf("\""));
			return TopS;
        }catch(Exception ex){
			ex.printStackTrace();
			return "";
        }
    }
	/////////////////////////////////////////////////////////////
	
	public static String start_a_container(String Id, String IP){
        try{
			String mainCommand = "curl -X POST http://" + IP + ":4243/containers/" + Id + "/start";
			//System.out.println(mainCommand);
			String[] command={"/bin/bash", "-c", mainCommand};
			Process P = Runtime.getRuntime().exec(command);
            P.waitFor();
            BufferedReader StdInput = new BufferedReader(new InputStreamReader(P.getInputStream()));
            String TopS ="";
			TopS= StdInput.readLine();
			return TopS;
        }catch(Exception ex){
			ex.printStackTrace();
			return "";
        }
    }
	/////////////////////////////////////////////////////////////
	
	public static String get_ListOfContainers(String IP){
		try{
			String mainCommand = "curl -X GET http://" + IP + ":4243/containers/json";
			String[] command={"/bin/bash", "-c", mainCommand};
			Process P = Runtime.getRuntime().exec(command);
			P.waitFor();
			BufferedReader Input = new BufferedReader(new InputStreamReader(P.getInputStream()));
			String ListOfContainers = Input.readLine();
			ListOfContainers = ListOfContainers.substring(1, ListOfContainers.length()-1);
			return ListOfContainers;
		}catch(Exception ex){
			ex.printStackTrace();
			return "";
        }
	}
	/////////////////////////////////////////////////////////////
	
	public static int findClosingBracketMatchIndex(String str, int pos) {
		int depth = 1;
		for (int i = pos + 1; i < str.length(); i++) {
			switch (str.charAt(i)) {
				case '{':
					depth++;
					break;
				case '}':
					if (--depth == 0) {
						return i;
					}
					break;
			}
		}
		return -1; // No matching closing parenthesis
	}
	/////////////////////////////////////////////////////////////
	
	public static String get_container_id(String PrivatePort, String PublicPort, String Type, String IP){
		String ListOfContainers = get_ListOfContainers(IP);
		int i=-1;
		while (i!=ListOfContainers.length()-1){
			i = findClosingBracketMatchIndex(ListOfContainers, 0);
			String tmp = ListOfContainers.substring(0, i+1);
			String match = "\"PrivatePort\":" + PrivatePort + ",\"PublicPort\":" + PublicPort + ",\"Type\":\"" + Type + "\"";
			if (tmp.contains(match)){
				tmp = tmp.substring(7, 71);
				return tmp;
			}
			if (i==ListOfContainers.length()-1) break;
			ListOfContainers = ListOfContainers.substring(i+2, ListOfContainers.length());
		}
		return "";
	}
	/////////////////////////////////////////////////////////////
	
	public static void stop_a_container(String Id, String IP){
        try{
			String mainCommand = "curl -X POST http://" + IP + ":4243/containers/" + Id + "/stop";
			String[] command={"/bin/bash", "-c", mainCommand};
			Process P = Runtime.getRuntime().exec(command);
            P.waitFor();
            BufferedReader StdInput = new BufferedReader(new InputStreamReader(P.getInputStream()));
            String TopS ="";
			while((TopS= StdInput.readLine())!=null){}
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
	/////////////////////////////////////////////////////////////
	
}
