# Input of On/Offloading Agent
<p align="justify">
According to the typical scenario explained before, each containerized application can include two parts: (i) aggregating data and (ii) processing data. While an application is containerized, a container includes two parts. One part is employed to aggregate data, and another part is used to process data. The On/Offloading Agent which responds to the Mobile On/Offload Processing component’s requests (e.g. start or stop requests) is able to launch or terminate container instances. In the previous section, different conditions were explained. Regardless of where a container is started (whether on the edge node or cloud), three different container instantiations may happen:
</p>

* When a container is launched, both aggregating and processing data should be performed.
* When a container is launched, only aggregating data needs to be executed.
* When a container is launched, only processing data should be performed.

<p align="justify">
The communication protocol used to transfer the data between the aggregating data part and the processing data part can be either TCP or UDP, depending on the application type. Since on one edge node or on one cloud-based host, more than one container may need to be running, port numbers on one resource should be managed. Because, same port cannot be accessed by multiple containers co-located on the same edge node or on the same cloud-based host. As a consequence, at least one port used to communicate the data aggregating part and the data processing part should be defined. In addition to this port, the application itself may need to access to a set of other ports in order to perform its functionalities. Therefore, a set of ports should be defined when a container has to be launched.
</p>

Each start or stop request which is a string includes different elements. The general form of a start or stop request defined for the On/Offloading Agent is as follows:
<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/6.png" width="60%"></p>

Note 1: Three different values should be determined for each port. For example: <br/> 
`6790,6790,tcp` -> means mapping TCP port 6790 in the container to port 6790 on the Docker host <br/>
`80,8080,tcp` -> means mapping TCP port 80 in the container to port 8080 on the Docker host <br/>
`6790,6790,udp` -> means mapping UDP port 6790 in the container to port 6790 on the Docker host <br/>
`3333,6790,udp` -> means mapping UDP port 3333 in the container to port 6790 on the Docker host <br/>

Note 2: The application may have different inputs (determined at the end of a request, see the above figure) depending on the service type. These inputs are defined by application developers. For example, in the logistic use case, one input can be threshold defined for the acceleration at the rate of 0.3 G: `Threshold,0.3` <br/>
Or for a video service use case, one input can be resolution value that is appropriate to current streaming data: `Resolution,480`

<p align="justify">
Note 3: If the request is going to be used for terminating (stop) a container, all elements defined in a request are not required to be determined.
</p>

<p align="justify">
Note 4: In a request, the order of elements is important.
</p>

The following three examples are presented to run the logistic application:
## Example 1
To reach the following situation, this request should be sent to the On/Offloading Agent:

* `"start,194.349.1.175,salmant/cvs_container_image_tcp:latest,1,6790,6790,tcp,Aggregate,yes,Process,yes,ProcessIP,127.0.0.1,Threshold,0.3,FileName,RAW_ACCELEROMETERS.csv,Y_column,6,Z_column,7,Interval,5,DB_IP,194.249.1.42,Driver_ID,782369"`

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/7.png" width="25%"></p>

## Example 2
To reach the following situation from example 1, these three successive requests should be sent to the On/Offloading Agent, respectively:

* `"stop,194.349.1.175,salmant/cvs_container_image:latest,1,6790,6790,tcp"`
* `"start,83.77.2.48,salmant/cvs_container_image:latest,1,6790,6790,tcp,Aggregate,no,Process,yes,ProcessIP,83.77.2.48,Threshold,0.3,FileName,RAW_ACCELEROMETERS.csv,Y_column,6,Z_column,7,Interval,5,DB_IP,194.249.1.42,Driver_ID,782369"`
* `"start,194.349.1.175,salmant/cvs_container_image:latest,1,6790,6790,tcp,Aggregate,yes,Process,no,ProcessIP,83.77.2.48,Threshold,0.3,FileName,RAW_ACCELEROMETERS.csv,Y_column,6,Z_column,7,Interval,5,DB_IP,194.249.1.42,Driver_ID,782369"`

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/8.png" width="45%"></p>

## Example 3
To reach the following situation from example 1, these two successive requests should be sent to the On/Offloading Agent, respectively:

* `"stop,194.349.1.175,salmant/cvs_container_image:latest,1,6790,6790,tcp"`
* `"start,83.77.2.48,salmant/cvs_container_image_tcp:latest,1,6790,6790,tcp,Aggregate,yes,Process,yes,ProcessIP,127.0.0.1,Threshold,0.3,FileName,RAW_ACCELEROMETERS.csv,Y_column,6,Z_column,7,Interval,5,DB_IP,194.249.1.42,Driver_ID,782369"`

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/9.png" width="35%"></p>
