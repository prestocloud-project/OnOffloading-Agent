# Typical scenario

<p align="justify">
The typical scenario is a system which includes sensors able to measure some parameters and send the raw data to a container-based application running on an edge node. The application consists of two parts: (i) aggregating data and (ii) processing data. In this scenario, icons represent as follows:
</p>

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/1.png" width="40%"></p>

<p align="justify">
In such a scenario, the part used to aggregate data receives the raw streaming data, collects and sends the aggregated data to another part which is used to process the data. In this case, both aggregating and processing data are performed in the container running on the edge node.
</p>

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/2.png" width="25%"></p>

<p align="justify">
For example in a logistic use case, all sensors such as accelerometer and magnetometer are connected to the edge node. In the edge node, data is aggregated and processed by the application. This application is useful in order to notify stakeholders (e.g. driver via alerts on a mobile phone or tablet) on situations where a possible accident may occur or attention is required.
</p>

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/3.png" width="25%"></p>

<p align="justify">
There are conditions where an edge node is not able to provide computing operations any more. For example if it is overloaded due to an increase in the number of sensors connected to the system during execution or the edge node is going to run out of storage capacity. In such conditions, the part which is used to process the data should be run on the cloud. In some cases, sensors are physically connected to the edge node, and the part used to aggregate the data needs to be run on the edge node. Therefore in this case, the edge node operates as intermediary to transmit the data to the cloud for data processing. 
</p>
<p align="justify">
An On/Offloading Agent is installed on every edge node. In such conditions, On/Offloading Agent responds to the Mobile On/Offload Processing component’s requests for the on/offloading tasks. In this case, the request is terminating (stop) the processing data on the edge node and launching (start) the processing data on the cloud. In other words, if this condition happens, there will be two containers. One container is employed to aggregate data on the edge node, and another one is used to process data on the cloud. To this end three successive steps should be accomplished by the On/Offloading Agent: (1) stop the current container instance running on the edge node, (2) start a new container instance on the cloud that includes the data processing part, and (3) start a new container instance on the edge node that includes the data aggregating part.
</p>

<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/4.png" width="45%"></p>

<p align="justify">
In some other cases, the part employed to aggregate data should also start working on the cloud in addition to the part used to process data. In such situations, On/Offloading Agent responds to the Mobile On/Offload Processing component’s request which is stop both aggregating and processing data on the edge node and start them on the cloud. In this case, both aggregating and processing data are executed in the container running on the cloud. To this end two successive steps should be accomplished by the On/Offloading Agent: (1) stop the current container instance running on the edge node, (2) start a new container instance on the cloud that includes both data aggregating part and data processing part.
</p>


<p align="center"><img src="https://github.com/salmant/PrEstoCloud/blob/master/OnOffloading/img/5.png" width="30%"></p>

