// ------------------------------------------------------------------------
// Author: Salman Taherizadeh - Jozef Stefan Institute (JSI)
// ------------------------------------------------------------------------
// wget http://www.java2s.com/Code/JarDownload/java/java-json.jar.zip
// unzip java-json.jar.zip
// # CLASSPATH for java-json.jar
// ------------------------------------------------------------------------
// args[0]: The port used to communicate the data aggregating part and the data processing part 
// args[1]: IP address where the data processing part is running
// args[2],...,args[n]: Application inputs, if any
// ------------------------------------------------------------------------
import java.io.*;
import java.net.*;
// ------------------------------------------------------------------------
....... application-specific libraries .......
// ------------------------------------------------------------------------
class AggregateData_TCP {
	public static void main(String args[]) throws Exception {
		try{
			// ------------------------------------------------------------------------
			Socket clientSocket = new Socket(args[1], Integer.parseInt(args[0])); // such as "127.0.0.1" - 6789
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			// ------------------------------------------------------------------------
			....... application-specific code .......
			// ------------------------------------------------------------------------
			clientSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
